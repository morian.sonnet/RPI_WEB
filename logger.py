import threading as thrd
import subprocess
import os
import time
from collections import deque
import psutil


class logger_thread(thrd.Thread):
    def __init__(self, main_thread, headline="timestamp time humidity temperature", RAM_limit = 1000000, space_limit = 20000000):
        super().__init__()
        self.mountpath ="/media/usb0"
        self.data_folder = "data"
        self.data = deque([])
        self.alread_saved = 0
        self.logging_should_run = True
        self.USB_connected = False
        self.log_file = ""
        self.RAM_limit = RAM_limit
        self.space_limit = space_limit
        self.headline = headline
        self.main_thread = main_thread
        self.new_start_time()

    def run(self):
        while self.logging_should_run:
            self.main_thread.publish_USB_status(self.USB_connected)
            while not self.USB_connected: 
                time.sleep(0.1)

                self.USB_connected = self.checkUSBstate()

                RAM_left = self.free_RAM() - self.RAM_limit
                if RAM_left < 0:
                    self.drop_data(count = 10)

                if not self.logging_should_run:
                    return


            self.check_create_folder()
            self.create_logfile()
            self.main_thread.publish_USB_status(self.USB_connected)

            while self.USB_connected and self.logging_should_run:
                time.sleep(0.1)

                free_space_local=self.free_space()
                if free_space_local < self.space_limit:
                    print("Not enough free space {} on the usb stick anymore".format(free_space_local))
                    continue
                data_length = len(self.data)
                if(data_length > 0):
                    self.save_data(min(data_length,10000),check=False)

                if not self.logging_should_run:
                    return

    def shutdown(self):
        self.logging_should_run=False
        self.join()


    """
    this function is supposed to be called from outside the thread.
    thread safety is given by deque, when only append and popleft is used.
    """
    def add_data(self,newdata):
        self.data.append(newdata)

    """
    function for checking whether usb stick is connected (and mounted).

    The test is only done by checking for mounted usb stick. Thus, automatic mounting is necessary to make this function work. Usage of usbmount is recommended

    @return True, if connected
    """
    def checkUSBstate(self): 
        return subprocess.call("lsblk | grep '"+self.mountpath+"'",shell=True)==0 #this is assuming that usb stick is mounted into /media/usb0

    def save_data(self,count=1,check=True):
        if check:
            data_length = len(self.data)
            if count > data_length:
                print("Can not save that many elements")
                count = data_length
        try:
            with open(self.log_file,'a') as f:
                for i in range(count):
                    element = self.data.popleft()
                    line=""
                    for j in range(len(element)):
                        line += "{} ".format(element[j])
                    line += '\n'
                    f.write(line)
        except IOError:
            print("Some error happened during working with the logging file, assuming USB stick is not connected anymore")
            self.USB_connected = False

    def create_logfile(self, number = 0):
        try:
            if os.path.isfile(self.mountpath+"/"+self.data_folder+"/"+"log_file"+str(number)+".txt"):
                self.create_logfile(number + 1)
            else:
                with open(self.mountpath+"/"+self.data_folder+"/"+"log_file"+str(number)+".txt",'w') as f:
                    f.write(self.headline+"\n")
                    f.write(self.timeline+"\n")
                self.log_file = self.mountpath+"/"+self.data_folder+"/"+"log_file"+str(number)+".txt"
        except IOError:
            print("Error at checking or creating logging file")

    def drop_data(self, count=1, check=True):
        if check:
            data_length = len(self.data)
            if count > data_length:
                print("Can not drop that many elements")
                count = data_length
        for i in range(count):
            self.data.popleft()

    def check_create_folder(self):
        try:
            if not os.path.exists(self.mountpath+"/"+self.data_folder):
                os.makedirs(self.mountpath+"/"+self.data_folder)
        except IOError:
            print("Error at checking or creating data folder")

    """
    function for checking the free space on the usb stick.

    @return the free space on the usb stick mounted at the expected mounting path in bytes
    """
    def free_space(self):
        mountpath_modified = self.mountpath.replace("/",r"\/")
        proc = subprocess.Popen("df -B 1 | awk '/"+mountpath_modified+"/ {print $4;}'",stdin=subprocess.PIPE,stdout=subprocess.PIPE,stderr=subprocess.STDOUT, close_fds=True, shell=True)
        time.sleep(0.1)
        result = proc.stdout.read().decode()
        if result.count('\n')==0:
            print("Could not find the mounting path while checking for free space")
            self.USB_connected = False
            return 0
        else:
            return int(result)

    def free_RAM(self):
        return psutil.virtual_memory().available
    
    def new_start_time(self):
        self.timeline = "started at " + self.main_thread.new_start_time()

    def restart_logging(self):
        self.USB_connected = False







