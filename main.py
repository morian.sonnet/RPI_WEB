import threading as thrd
from time import time, sleep
import numpy as np
import logger
from random import random
from flask_socketio import SocketIO, emit
import datetime
from collections import deque
import Adafruit_SHT31 as SHT


class main_thread(thrd.Thread):
    def __init__(self, socket, sensor_address = 0x44):
        super().__init__()
        self.sleep_gen = self.sleep_time_generator()
        self.period = 1.0
        self.data = deque([])
        self.main_should_run = True
        self.socket = socket
        self.logger_thread = logger.logger_thread(main_thread = self)
        self.logger_thread.new_start_time()
        self.logger_thread.start()
        self.log_timer = 10

        self.sensor_address = sensor_address
        try:
            self.sensor = SHT.SHT31(address = self.sensor_address)
            self.sensor.read_temperature() #To try wether connection works, if does not already at start, fail
        except OSError:
            print("Could not connect to sensor! ALERT")
            #raise  Do not make problems, otherwise raspi need to be restarted if not connected to sensor at start

    def set_log_timer(self,new_log_timer):
        self.log_timer = new_log_timer


    def new_start_time(self):
        self.t0 = time()
        return datetime.datetime.fromtimestamp(self.t0).strftime("%Y-%m-%d-%H-%M-%S.%f")

    def get_last_dataset(self):
        if(len(self.data)>0):
            return self.data[-1]
        else:
            return {'time':0.0,'temp':0.0,'hum':0.0}

    def shutdown(self):
        self.logger_thread.shutdown()
        sleep(0.4)
        self.main_should_run = False
        self.join()

    def run(self):
        log_counter = 0
        while self.main_should_run:
            new_data = self.get_temp_hum()
            time_point = time()
            time_stamp = datetime.datetime.fromtimestamp(time_point).strftime("%Y-%m-%d-%H-%M-%S.%f")
            new_data_dict = {'time':time_point,'temp':new_data[0],'hum':new_data[1]}

            log_counter += 1
            if log_counter >= self.log_timer:
                log_counter = 0
                new_data_log = [time_stamp,time_point - self.t0,new_data[0],new_data[1]]
                self.logger_thread.add_data(new_data_log)
                self.data.append(new_data_dict)
                new_data_dict['should_display']=True
            else:
                new_data_dict['should_display']=False
            self.socket.emit('newdata',new_data_dict, namespace='/data')

            if(len(self.data)>1000):
                self.data.popleft()
            sleep(self.sleep_gen.__next__())

    def send_all(self):
        self.socket.emit('alldata',[msg for msg in self.data],namespace='/data')

    def set_period(self,new_period):
        self.period = new_period


    def sleep_time_generator(self):
        t0 = time()
        wait_time_so_far = 0.0
        while True:
            period_copy = self.period
            wait_time_so_far += period_copy
            to_wait = t0 + wait_time_so_far - time()
            if(to_wait < 0.0):
                t0 = time()
                wait_time_so_far = 0.0
                yield 0.0
            else:
                yield t0 + wait_time_so_far - time()

    def get_temp_hum(self, try_out=10):
        try:
            temp = self.sensor.read_temperature()
            hum = self.sensor.read_humidity()
        except OSError:
            print("OSError, Could non read data from sensor")
            if try_out <=0:
                temp=0.0
                hum=0.0
            else:
                return self.get_temp_hum(try_out-1)
        return [temp,hum]

    def publish_USB_status(self,status):
        print("publish USB status"+str(status))
        self.socket.emit('usbstatus',status, namespace='/data')
    

