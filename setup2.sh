#! /bin/bash
apt-get install python3-psutil -y || exit 1
pip3 install flask_socketio Adafruit_GPIO --no-cache-dir || exit 1
python3 application.py
