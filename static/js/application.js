var config = {
    type: 'scatter',
    data: {
        datasets: [{
            label: 'Temperature',
            backgroundColor: window.chartColors.red,
            borderColor: window.chartColors.red,
            data: [
            ],
            fill: false,
            yAxisID: 'y-axis-left'
        },{
            label: 'Humidity',
            backgroundColor: window.chartColors.green,
            borderColor: window.chartColors.green,
            data: [
            ],
            fill: false,
            yAxisID: 'y-axis-right'
        }]
    },
    options: {
        responsive: true,
        title: {
            display: false,
            text: 'Temperature and Humidity'
        },
        tooltips: {
            mode: 'index',
            intersect: false,
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        showLines: true,
        scales: {
            xAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Time'
                },
                ticks: {
                    callback: timestamp
                }
            }],
            yAxes: [{
                display: true,
                position: 'left',
                id: 'y-axis-left',
                ticks: {
                    fontColor: window.chartColors.red,
                },
                scaleLabel: {
                    display: true,
                    labelString: 'Temperature (°C)'
                }
            },{
                display: true,
                position: 'right',
                id: 'y-axis-right',
                ticks: {
                    fontColor: window.chartColors.green,
                    min: 0.0,
                    max: 100.0
                },
                scaleLabel: {
                    display: true,
                    labelString: 'Humidity (%RH)'
                }
            }]
        }
    }
};

function timestamp(label,index,labels){
	var date = new Date(label*1000);
	var hours = '0'+date.getHours();
	var minutes = '0'+date.getMinutes();
	var seconds = '0'+date.getSeconds();
	return hours.substr(-2) + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
}

function exact_timestamp(time){
	var date = new Date(time*1000);
	var hours = '0'+date.getHours();
	var minutes = '0'+date.getMinutes();
	var seconds = '0'+Math.round((time%60)*1000)/1000;
	return hours.substr(-2) + ':' + minutes.substr(-2) + ':' + seconds.substr(-6);
}

window.onload = function() {
    var ctx = document.getElementById('canvas').getContext('2d');
    window.myLine = new Chart(ctx, config);
};

function change_period() {
    console.log("Changing period")
    var socket = io.connect('http://' + document.domain + ':' + location.port + '/config');
    //socket.emit('period',"ulf")
    socket.emit('period',document.getElementById("new_period").value)
}

function change_log_timer() {
    console.log("Changing log timer")
    var socket = io.connect('http://' + document.domain + ':' + location.port + '/config');
    //socket.emit('period',"ulf")
    socket.emit('log_timer',document.getElementById("new_log_timer").value)
}

function randomizeData() {
    config.data.datasets.forEach(function(dataset) {
        dataset.data = dataset.data.map(function() {
            return randomScalingFactor();
        });

    });

    window.myLine.update();
}

function add_data_to_chart(new_data) {
    config.data.datasets[0].data.push({x: new_data.time, y: new_data.temp}); //temperature
    config.data.datasets[1].data.push({x: new_data.time, y: new_data.hum}); //humidity
}

function update_chart() {
    window.myLine.update();
}

function delete_chart() {
    config.data.datasets[0].data=[];
    config.data.datasets[1].data=[];
}


$(document).ready(function(){
    //connect to the socket server.
    var socket = io.connect('http://' + document.domain + ':' + location.port + '/data');

    socket.on('newdata', function(msg) {
        console.log("Received new data " + msg.time +" "+ msg.temp +" "+ msg.hum + " "+ msg.should_display);
        data_string = '';
        data_string = data_string + '<p>' + "Time: " + exact_timestamp(msg.time) + '</p>';
        data_string = data_string + '<p>' + "Temperature: " + Math.round(msg.temp*100)/100 + '</p>';
        data_string = data_string + '<p>' + "Humidity: " + Math.round(msg.hum*100)/100 + '</p>';
	if(msg.should_display){
		add_data_to_chart(msg);
		update_chart();
	}
        $('#log').html(data_string);
    });
    socket.on('alldata', function(msg) {
        console.log("Received new all data message");
        for(var i=0; i<msg.length;i++){
            add_data_to_chart(msg[i]);
        }
        update_chart();

    });
    socket.on('deletechart', function() {
        console.log("Deleting chart");
        delete_chart();
        update_chart()
    });
    socket.on('usbstatus',function(status) {
	    console.log("Got new usb status")
        if(status==true){
            console.log("USB connected")
            $('#status').html("<p>USB status: connected</p>");
        } else {
            console.log("USB disconnected")
            $('#status').html("<p>USB status: disconnected</p>");
        }
    });

});

function reset_t0() {
    console.log("Resetting logging t0")
    var socket = io.connect('http://' + document.domain + ':' + location.port + '/config');
    //socket.emit('period',"ulf")
    socket.emit('resett0')
}





