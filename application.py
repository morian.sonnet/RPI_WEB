# Start with a basic flask app webpage.
from flask_socketio import SocketIO, emit
from flask import Flask, render_template, url_for, copy_current_request_context
from random import random
from time import sleep
from threading import Thread, Event
import main
import datetime



app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
app.config['DEBUG'] = True

#turn the flask app into a socketio app
socketio = SocketIO(app)

main_thread = main.main_thread(socketio)
main_thread.start()

@app.route('/')
@app.route('/index.html')
def index():
    data = main_thread.get_last_dataset()
    usb_status = main_thread.logger_thread.USB_connected
    usb_status_string = "connected" if usb_status else "disconnected"
    period = main_thread.period
    log_timer = main_thread.log_timer
    
    return render_template('index.html', usb_status = usb_status_string, time = datetime.datetime.fromtimestamp(data["time"]).strftime("%H:%M:%S.%f"), temp = "{:.2f}".format(data["temp"]), humidity = "{:.2f}".format(data["hum"]), period = period, log_timer = log_timer)

@socketio.on('connect', namespace='/data')
def data_connect():
    socketio.emit('deletechart',namespace='/data')
    main_thread.send_all()
    print('Client connected to data namespace')

@socketio.on('disconnect', namespace='/data')
def data_disconnect():
    print('Client disconnected from data namespace')

@socketio.on('period', namespace='/config')
def new_period(msg):
    print("Got a  new period "+str(msg))
    try:
        new_value = float(msg)
    except ValueError:
        return
    print("Transform into float succesful")
    main_thread.set_period(new_value)

@socketio.on('log_timer', namespace='/config')
def new_log_timer(msg):
    print("Got a  new log_timer "+str(msg))
    try:
        new_value = int(msg)
    except ValueError:
        return
    print("Transform into int succesful")
    main_thread.set_log_timer(new_value)

@socketio.on('resett0',namespace='/config')
def reset_t0():
    print("Resetting logging t0")
    main_thread.logger_thread.restart_logging()

socketio.run(app, host="0.0.0.0", use_reloader=False)


if __name__ == '__main__':
    pass
