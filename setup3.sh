#! /bin/bash
apt-get install dnsmasq hostapd -y || exit 1
systemctl stop dnsmasq
systemctl disable hostapd
cat setup_files/dhcpcd >> /etc/dhcpcd.conf
mv /etc/dnsmasq.conf /etc/dnsmasq.conf.orig
cat setup_files/dnsmasq >> /etc/dnsmasq.conf
cat setup_files/hostapd >> /etc/hostapd/hostapd.conf
sed "s+#DAEMON_CONF.*+DAEMON_CONF=\"/etc/hostapd/hostapd.conf\"+" /etc/default/hostapd -i
cat setup_files/hosts >> /etc/hosts
sed "/network={/,/}/d" -i /etc/wpa_supplicant/wpa_supplicant.conf
sed "s/#net.ipv4.ip_forward=1/net.ipv4.ip_forward=1/" /etc/sysctl.conf -i
iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
iptables-save > /etc/iptables.ipv4.nat
sed '$d' /etc/rc.local -i
cat setup_files/rc.local >> /etc/rc.local
echo "Everything is done"
read -n 1 -s
sudo reboot now
