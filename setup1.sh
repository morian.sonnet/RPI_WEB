#! /bin/bash
apt-get install usbmount vim -y || exit 1
mkdir -p /etc/systemd/system/systemd-udevd.service.d
cat setup_files/systemd-udevd >> /etc/systemd/system/systemd-udevd.service.d/override.conf
sed "s/FS_MOUNTOPTIONS=.*/FS_MOUNTOPTIONS=\"-fstype=vfat,gid=users,dmask=0007,fmask=0117\"/" /etc/usbmount/usbmount.conf -i
sudo reboot now
